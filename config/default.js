// See https://eslint.org/docs/rules/
// and https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin#supported-rules
module.exports = {
    env: {
        es6: true,
    },
    parser: "@typescript-eslint/parser",
    parserOptions: {
        project: "tsconfig.json",
        sourceType: "module",
    },
    plugins: ["@typescript-eslint", "prettier"],
    rules: {
        "@typescript-eslint/array-type": [
            "error",
            {
                default: "array-simple",
            },
        ],
        "@typescript-eslint/consistent-type-definitions": "off",
        "@typescript-eslint/indent": "off",
        "@typescript-eslint/member-delimiter-style": [
            "off",
            "error",
            {
                multiline: {
                    delimiter: "none",
                    requireLast: true,
                },
                singleline: {
                    delimiter: "semi",
                    requireLast: false,
                },
            },
        ],
        "@typescript-eslint/quotes": ["error", "double", {
            allowTemplateLiterals: true,
            avoidEscape: true
        }],
        "@typescript-eslint/semi": ["error", "always"],
        "@typescript-eslint/space-within-parens": ["off", "never"],
        "@typescript-eslint/type-annotation-spacing": "off",
        "arrow-parens": ["off", "as-needed"],
        camelcase: "off",
        "eol-last": "error",
        "id-blacklist": "off",
        "id-match": "off",
        "import/no-extraneous-dependencies": "off",
        "import/order": "off",
        "linebreak-style": "off",
        "quotes": "off",
        "max-len": [
            "error",
            {
                ignorePattern: "^import .*?,.*?\\{.*?\\}|^import \\{|^export ",
                ignoreUrls: true,
                code: 130, // Should match with .prettier.json
            },
        ],
        "new-parens": "off",
        "newline-per-chained-call": "off",
        "no-extra-semi": "error",
        "no-irregular-whitespace": "off",
        "no-multiple-empty-lines": "error",
        "no-trailing-spaces": "off",
        "no-underscore-dangle": "off",
        "quote-props": "off",
        "space-before-function-paren": "off",
        "prettier/prettier": "error",
    },
};
