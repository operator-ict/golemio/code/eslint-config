# @golemio/eslint-config

Golemio ESLint Shareable Config

## Installation

```bash
npm install --save-dev @golemio/eslint-config typescript@^4.4.4 eslint@^8.1.1 prettier@^2.5.1
```

## Usage

```javascript
// .eslintrc.js
module.exports = {
    extends: "@golemio/eslint-config",
};
```

## Tests

Clone the repo, `npm install`, and run `npm run test`
