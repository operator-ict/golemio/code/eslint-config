# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.2] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.1] - 2022-06-14

### Changed

-   Migrate to npm
-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2021-03-16

### Changed

-   Update dependencies
-   Sync gitlab ci config with other modules

## [1.0.2] - 2021-01-11

### Changed

-   Update README.md

## [1.0.1] - 2021-01-11

### Changed

-   Remove the badge in README.md
-   Write CHANGELOG manually

## [1.0.0] - 2021-01-11

### Added

-   Create ESLint shareable configuration
-   Write CI configuration, deploy to npmjs

