import { promisify } from "util";

const sleep = promisify(setTimeout);

const LONG_STRING =
    "long string long string long string long string long string long string long string long string long string long string";

namespace DummyNamespace {
    export type DummyType = "string1" | typeof LONG_STRING;
}

interface IDummyInterface {
    getSomething: () => Promise<DummyNamespace.DummyType[]>;
    getSomethingElse: () => Array<Promise<Record<string, string>>>;
}

class DummyClass implements IDummyInterface {
    private static SOME_CONSTANT: DummyNamespace.DummyType = "string1";

    public async getSomething() {
        try {
            await sleep(1);
        } catch (err) {
            console.error(err.message);
        }

        return [DummyClass.SOME_CONSTANT];
    }

    public getSomethingElse() {
        const promise: Promise<Record<string, string>> = new Promise((resolve) =>
            resolve({
                key1: "value1",
                key2: "value2",
            })
        );

        return [promise];
    }
}

export { DummyNamespace, DummyClass };
